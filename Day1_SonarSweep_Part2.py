import queue
import sys
import queue

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    num_increased = 0
    last_sum = 0
    current_sum = 0
    window = queue.Queue()
    with open(file_path, 'r') as f:
        for line in f:
            current = int(line)
            current_sum += current
            window.put(current)
            size = window.qsize()
            if window.qsize() == 4:
                to_remove = window.get()
                current_sum -= to_remove
                if current_sum > last_sum:
                    num_increased += 1
                last_sum = current_sum
            else:
                last_sum += current

    print(num_increased)
