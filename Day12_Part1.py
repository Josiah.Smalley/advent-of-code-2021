import re
from collections import defaultdict

from Common.FileHelper import iterate_over_file

adj = defaultdict(set)
small_caves = set()


def process_line(line):
    node1, node2 = re.match(r"(.+)-(.+)$", line).groups()
    adj[node1].add(node2)
    adj[node2].add(node1)
    if node1.islower():
        small_caves.add(node1)
    if node2.islower():
        small_caves.add(node2)


def find_paths(already_seen, current_path, all_paths):
    current_node = current_path[-1]

    if current_node == 'end':
        all_paths.add(current_path)
        return

    for neighbor in adj[current_node]:
        if neighbor in already_seen and neighbor in small_caves:
            continue
        find_paths(already_seen.union({neighbor}), (*current_path, neighbor), all_paths)


if __name__ == '__main__':
    iterate_over_file(process_line)
    every_path = set()
    find_paths({'start'}, ('start',), every_path)

    print(len(every_path))
