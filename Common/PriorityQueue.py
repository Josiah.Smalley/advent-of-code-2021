import heapq
from itertools import count


class PriorityQueue:
    def __init__(self, removed_element='<removed-element>'):
        self.queue = []
        self.entries = dict()
        self.counter = count()
        self.removed_element = removed_element

    def is_empty(self):
        return 0 == len(self.entries)

    def add_element(self, element, priority=0):
        if element in self.entries:
            self.remove_element(element)
        element_count = next(self.counter)
        entry = [priority, element_count, element]
        self.entries[element] = entry
        heapq.heappush(self.queue, entry)

    def remove_element(self, element):
        entry = self.entries.pop(element)
        entry[-1] = self.removed_element

    def pop_element(self):
        while self.queue:
            priority, element_count, element = heapq.heappop(self.queue)
            if element is not self.removed_element:
                del self.entries[element]
                return element
        raise KeyError("Attempted to pop from an empty priority queue")

    def __str__(self):
        return str(self.queue)
