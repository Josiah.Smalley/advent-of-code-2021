class Node:
    def __init__(self, value):
        self.parent = value
        self.rank = 0

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "{parent: " + str(self.parent) + ", rank: " + str(self.rank) + "}"


class DisjointSet:
    def __init__(self):
        self.nodes = dict()

    def __str__(self):
        return str(self.nodes)

    def make_set(self, value):
        if value in self.nodes:
            return

        self.nodes[value] = Node(value)

    def are_connected(self, x, y):
        return self.find(x) == self.find(y)

    def find(self, value):
        parent = self.get_parent(value)
        while parent != value:
            self.set_parent(value, self.get_parent(parent))
            value = self.get_parent(value)
            parent = self.get_parent(value)
        return value

    def merge(self, x, y):
        x = self.find(x)
        y = self.find(y)

        if x == y:
            return False

        if self.get_rank(x) < self.get_rank(y):
            x, y = y, x

        self.set_parent(y, x)
        if self.get_rank(x) == self.get_rank(y):
            self.set_rank(x, self.get_rank(x) + 1)

        return True

    def get_parent(self, value):
        return self.nodes[value].parent

    def get_rank(self, value):
        return self.nodes[value].rank

    def set_parent(self, value, parent):
        self.nodes[value].parent = parent

    def set_rank(self, value, rank):
        self.nodes[value].rank = rank
