from Common.FileHelper import map_file_lines
from functools import reduce

open_close = {'(': ')', '{': '}', '[': ']', '<': '>'}
close_open = {end: start for start, end in open_close.items()}

autocomplete_score = {'(': 1, '[': 2, '{': 3, '<': 4}


def process_line(the_line):
    stack = []
    for c in the_line:
        if c in open_close:
            stack.append(c)
            continue

        if len(stack) == 0 or stack[-1] != close_open[c]:
            return c, None

        stack.pop()

    return None, stack


def score_completion(stack):
    return reduce(
        lambda result, current: result * 5 + autocomplete_score[current],
        stack[::-1],
        0
    )


if __name__ == '__main__':

    scores = list(
        sorted(
            map(
                lambda incomplete_stack: score_completion(incomplete_stack),
                [the_stack for _, the_stack in map_file_lines(process_line) if the_stack is not None]
            )
        )
    )

    print(scores[len(scores) // 2])
