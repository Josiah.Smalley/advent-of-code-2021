import sys
import re
from collections import defaultdict


def process_number(number, boards, value_map):
    with_bingos = set()
    for (i, r, c) in value_map[number]:
        boards[i][r][c] = -1
        if has_bingo(boards[i], r, c):
            with_bingos.add(i)
    return with_bingos


def has_bingo(board, r, c):
    def was_called(x):
        return x == -1

    return all(map(was_called, board[r])) or all(map(was_called, [board_row[c] for board_row in board]))


def calculate_score(board, number):
    return number * sum(map(lambda row: sum(filter(lambda x: x != -1, row)), board))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    bingo_boards = []
    val_map = defaultdict(set)
    with open(file_path, 'r') as f:
        drawn_numbers = list(map(int, f.readline().split(',')))
        index = 0
        while True:
            line = f.readline()
            if line == "":
                break
            board = [list(map(int, re.findall(r'\d+', f.readline()))) for _ in range(5)]
            bingo_boards.append(board)
            for row in range(len(board)):
                for col in range(len(board[row])):
                    val_map[board[row][col]].add((index, row, col))

            index += 1

    all_boards = set(range(len(bingo_boards)))
    all_boards_with_bingos = set()
    last_board = -1
    for number in drawn_numbers:
        boards_with_bingos = process_number(number, bingo_boards, val_map)
        all_boards_with_bingos = all_boards_with_bingos.union(boards_with_bingos)
        if len(all_boards_with_bingos) + 1 == len(all_boards):
            last_board = all_boards.difference(all_boards_with_bingos).pop()
            continue
        if last_board in all_boards_with_bingos:
            print(calculate_score(bingo_boards[last_board], number))
            break
