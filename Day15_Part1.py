from Common.FileHelper import map_file_lines

if __name__ == '__main__':
    risk = map_file_lines(lambda line: [int(c) for c in line])

    size = len(risk)

    dp = [[0 for _ in range(size)] for __ in range(size)]

    diag = 1
    max_diag = 2 * (size - 1) + 1

    while diag < max_diag:
        row = diag if diag < size else size - 1
        while row > -1:
            col = diag - row
            if col >= size:
                row -= 1
                continue

            from_left = dp[row][col - 1] if col > 0 else float('inf')
            from_up = dp[row - 1][col] if row > 0 else float('inf')

            dp[row][col] = risk[row][col] + min(from_up, from_left)

            row -= 1

        diag += 1

    print(dp[-1][-1])
