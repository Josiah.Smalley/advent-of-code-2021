from Common.FileHelper import map_file_lines

open_close = {'(': ')', '{': '}', '[': ']', '<': '>'}
close_open = {end: start for start, end in open_close.items()}

corrupt_score = {')': 3, ']': 57, '}': 1197, '>': 25137}


def process_line(the_line):
    stack = []
    for c in the_line:
        if c in open_close:
            stack.append(c)
            continue

        if len(stack) == 0 or stack[-1] != close_open[c]:
            return c, None

        stack.pop()

    return None, stack


if __name__ == '__main__':
    print(sum(map(lambda tup: corrupt_score[tup[0]],
                  filter(lambda tup: tup[0] is not None, map_file_lines(process_line)))))
