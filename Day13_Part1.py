import re
import sys

from Common.FileHelper import iterate_over_file

dots = set()
folds = []


def process_line(line):
    if line == '':
        return

    pair_match = re.match(r"^(\d+),(\d+)$", line)
    if pair_match is not None:
        dots.add(tuple(map(int, pair_match.groups())))
        return

    axis, value = re.match(r"^fold along (.)=(\d+)$", line).groups()
    folds.append((axis, int(value)))


if __name__ == '__main__':
    iterate_over_file(process_line)

    num_steps = int(sys.argv[2]) if len(sys.argv) > 2 else len(folds)
    step = 0
    while step < num_steps:
        axis, value = folds[step]
        step += 1
        new_dots = set()
        for x, y in dots:
            # print("Resolving", x, y)
            if axis == 'x':
                new_dot = (value - abs(value - x), y)
            else:
                new_dot = (x, value - abs(value - y))
            new_dots.add(new_dot)
            # print("\tMapped to:", new_dot)
        dots = new_dots
    # print(dots)
    print(len(dots))
