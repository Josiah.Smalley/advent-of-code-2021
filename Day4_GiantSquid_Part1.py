import sys
import re
from collections import defaultdict


def process_number(number, boards, value_map):
    for (i, r, c) in value_map[number]:
        boards[i][r][c] = -1
        if has_bingo(boards[i], r, c):
            return calculate_score(boards[i], number)
    return False


def has_bingo(board, r, c):
    def was_called(x):
        return x == -1

    return all(map(was_called, board[r])) or all(map(was_called, [board_row[c] for board_row in board]))


def calculate_score(board, number):
    return number * sum(map(lambda row: sum(filter(lambda x: x != -1, row)), board))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    bingo_boards = []
    val_map = defaultdict(set)
    with open(file_path, 'r') as f:
        drawn_numbers = list(map(int, f.readline().split(',')))
        index = 0
        while True:
            line = f.readline()
            if line == "":
                break
            board = [list(map(int, re.findall(r'\d+', f.readline()))) for _ in range(5)]
            bingo_boards.append(board)
            for row in range(len(board)):
                for col in range(len(board[row])):
                    val_map[board[row][col]].add((index, row, col))

            index += 1

    for number in drawn_numbers:
        result = process_number(number, bingo_boards, val_map)
        if result:
            print(result)
            break
