import sys
from collections import defaultdict


def split_and_sort(list_of_digits):
    return list(
        map(
            lambda val: ''.join(sorted(list(val))),
            list_of_digits.split(' ')
        )
    )


def calculate_digit_map(lights):
    result = dict()
    backward = [None for _ in range(10)]

    def set_value(digit, string):
        result[string] = digit
        backward[digit] = string

    twoThreeFive = []
    zeroSixNine = []

    # find the easy ones
    for light in lights:
        length = len(light)
        if length == 2:  # "1"
            set_value(1, light)
        elif length == 3:  # "7"
            set_value(7, light)
        elif length == 4:  # "4"
            set_value(4, light)
        elif length == 5:
            twoThreeFive.append(light)
        elif length == 6:
            zeroSixNine.append(light)
        elif length == 7:  # "8"
            set_value(8, light)

    # find "3"
    seven_set = set(backward[7])
    three = list(filter(
        lambda v: seven_set.issubset(set(v)),
        twoThreeFive
    ))[0]
    twoThreeFive.remove(three)
    set_value(3, three)

    # find "9"
    three_set = set(three)
    nine = list(filter(lambda v: three_set.issubset(set(v)), zeroSixNine))[0]
    zeroSixNine.remove(nine)
    set_value(9, nine)

    # find "5"
    nine_set = set(nine)
    five = list(filter(lambda v: nine_set.issuperset(set(v)), twoThreeFive))[0]
    twoThreeFive.remove(five)
    set_value(5, five)
    # "2" is left
    set_value(2, twoThreeFive[0])

    # find "6"
    five_set = set(five)
    six = list(filter(lambda v: five_set.issubset(set(v)), zeroSixNine))[0]
    zeroSixNine.remove(six)
    set_value(6, six)
    # "0" is left
    set_value(0, zeroSixNine[0])

    return result


def decode_digits(raw_string, digit_map):
    return ''.join(map(lambda v: str(digit_map[v]), split_and_sort(raw_string)))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    total = 0
    with open(file_path, 'r') as f:
        for line in f:
            digits, display = line.strip().split(' | ')
            display_map = calculate_digit_map(split_and_sort(digits))
            total += int(decode_digits(display, display_map))

    print(total)
