import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    num_increased = 0
    last = float('inf')
    with open(file_path, 'r') as f:
        for line in f:
            current = int(line)
            if current > last:
                num_increased += 1
            last = current

    print(num_increased)
