from Common.FileHelper import map_file_lines


def string_to_digit_array(string):
    return list(map(int, string))


def is_smaller_than_surrounding(the_grid, row, col, max_row, max_col):
    value = the_grid[row][col]
    if row > 0 and value >= the_grid[row - 1][col]:
        return False
    if row + 1 < max_row and value >= the_grid[row + 1][col]:
        return False
    if col > 0 and value >= the_grid[row][col - 1]:
        return False
    if col + 1 < max_col and value >= the_grid[row][col + 1]:
        return False

    return True


if __name__ == '__main__':
    grid = map_file_lines(string_to_digit_array)
    num_rows = len(grid)
    num_cols = len(grid[0])

    risk_sum = 0

    for i in range(num_rows):
        for j in range(num_cols):
            if is_smaller_than_surrounding(grid, i, j, num_rows, num_cols):
                risk_sum += 1 + grid[i][j]

    print(risk_sum)