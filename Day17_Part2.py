import math
import re
from Common.FileHelper import map_file_lines
from itertools import product


def process_line(line):
    return map(int, re.match(r"^target area: x=(\d+)\.\.(\d+), y=(-\d+)\.\.(-\d+)$", line).groups())


def tri(n):  # triangular numbers
    return (n * (n + 1)) // 2


def get_tri_index(value):
    index = -0.5 + (0.25 + 2 * value) ** 0.5
    int_index = int(index)
    if int_index == index:
        return int_index
    return -1


def calc_x(v_x, t):
    if t <= v_x:
        return int(-t * t / 2 + (v_x + 0.5) * t)
    return tri(v_x)


def does_hit_region(v_x, v_y, x_min, x_max, y_min, y_max):
    t = 0
    x = 0
    y = 0
    if v_y > 0:
        # fast-forward past all y > 0
        t = v_y * 2 + 1
        x = calc_x(v_x, t)
        v_x = max(0, v_x - t)
        v_y = -v_y - 1

    while x <= x_max and y >= y_min:
        if x_min <= x and y <= y_max:
            return True
        x += v_x
        v_x = 0 if v_x == 0 else v_x - 1
        y += v_y
        v_y -= 1

    return False


if __name__ == '__main__':
    min_x, max_x, min_y, max_y = map_file_lines(process_line)[0]

    print(
        len(
            set(
                filter(lambda tup: does_hit_region(tup[0], tup[1], min_x, max_x, min_y, max_y),
                       product(range(math.ceil(-0.5 + math.sqrt(2 * min_x)), max_x + 1), range(min_y, -min_y)
                               )
                       )
            )
        )
    )
