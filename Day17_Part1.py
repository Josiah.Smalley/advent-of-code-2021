import math
import re
from Common.FileHelper import map_file_lines


def process_line(line):
    return map(int, re.match(r"^target area: x=(\d+)\.\.(\d+), y=(-\d+)\.\.(-\d+)$", line).groups())


def tri(n):  # triangular numbers
    return (n * (n + 1)) // 2


if __name__ == '__main__':
    min_x, max_x, min_y, max_y = map_file_lines(process_line)[0]
    # yay for calculus

    print(tri(-min_y - 1))
