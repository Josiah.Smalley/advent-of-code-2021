import sys
from collections import defaultdict


def find_position(counts):
    a = min(counts.keys())
    b = max(counts.keys())
    a_val = calculate_sign(counts, a)
    a_sign = sign(a_val)
    b_val = calculate_sign(counts, b)

    while a + 1 < b:
        c = (a + b) // 2
        c_val = calculate_sign(counts, c)
        c_sign = sign(c_val)
        if c_sign == 0:
            return c

        if a_sign == c_sign:
            a, a_val, = c, c_val
        else:
            b, b_val = c, c_val

    if a == b or abs(a_val) <= abs(b_val):
        return a

    return b


def calculate_sign(counts, x):
    return sum(map(lambda pair: pair[1] * sign(pair[0] - x), counts.items()))


def sign(x):
    if x == 0:
        return 0
    elif x < 0:
        return -1
    return 1


def calculate_cost(counts, x):
    return sum(map(lambda pair: pair[1] * abs(pair[0] - x), counts.items()))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    value_counts = defaultdict(int)

    with open(file_path, 'r') as f:
        for value in map(int, f.readline().split(',')):
            value_counts[value] += 1

    end_position = find_position(value_counts)
    print(calculate_cost(value_counts, end_position))

