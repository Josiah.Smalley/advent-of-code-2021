import math
import re

from Common.FileHelper import map_file_lines
from functools import reduce


def add_numbers(first: str, second: str) -> str:
    number = f'[{first},{second}]'
    while True:
        exploded = explode_first(number)
        if exploded != number:
            number = exploded
            continue
        split = split_first(number)
        if split != number:
            number = split
            continue
        break

    # print('Added', '\t' + first, '\t' + second, '\t' + number, sep='\n')
    return number


def explode_first(number: str) -> str:
    last_start = 0
    brackets = 0
    index = 0
    length = len(number)
    while index < length:
        c = number[index]
        if c == '[':
            last_start = index
            brackets += 1
        elif c == ']':
            brackets -= 1
            if brackets == 4:
                break
        index += 1

    if brackets != 4:
        return number

    exploded_start = last_start
    exploded_end = index
    left_number, right_number = map(int, number[last_start + 1:index].split(','))

    previous_number = None
    previous_number_start = 0
    previous_number_end = last_start - 1
    while previous_number_end >= 0 and not number[previous_number_end].isnumeric():
        previous_number_end -= 1
    if previous_number_end >= 0:
        previous_number_start = previous_number_end - 1
        while previous_number_start >= 0 and number[previous_number_start].isnumeric():
            previous_number_start -= 1
        previous_number = int(number[previous_number_start + 1:previous_number_end + 1])

    next_number = None
    next_number_start = index + 1
    next_number_end = length
    while next_number_start < length and not number[next_number_start].isnumeric():
        next_number_start += 1
    if next_number_start < length:
        next_number_end = next_number_start + 1
        while next_number_end < length and number[next_number_end].isnumeric():
            next_number_end += 1
        next_number = int(number[next_number_start:next_number_end])

    result = ''
    if previous_number is not None:
        result += number[:previous_number_start + 1]
        result += str(previous_number + left_number)
        result += number[previous_number_end + 1:exploded_start]
    else:
        result += number[:exploded_start]

    result += '0'

    if next_number is not None:
        result += number[exploded_end + 1:next_number_start]
        result += str(next_number + right_number)
        result += number[next_number_end:]
    else:
        result += number[exploded_end + 1:]

        # print(
        #     f'\t\tExploding {number} using ({previous_number} @ {previous_number_start}, {previous_number_end}), ({next_number} @ {next_number_start, next_number_end}), replacing: ({exploded_start, exploded_end} => {left_number, right_number})')
        # print(f'\t\tResult: {result}')

    return result


def split_first(number: str):
    found: re.Match = re.search(r"(\d{2,})", number)

    if not found:
        return number

    span = found.span()
    value = int(number[span[0]:span[1]])
    left_val = int(math.floor(value / 2))
    right_val = int(math.ceil(value / 2))
    replacement = f'[{left_val},{right_val}]'
    return number[:span[0]] + replacement + number[span[1]:]


def calculate_magnitude(number: str) -> int:
    stack = []
    last_value = None
    for c in number:
        if c.isdigit():
            last_value = int(c) if last_value is None else last_value * 10 + int(c)
        else:
            if last_value is not None:
                stack.append(last_value)
                last_value = None
            if c == ']':
                magnitude = 2 * stack.pop() + 3 * stack.pop()
                stack.append(magnitude)

    return stack[0]


if __name__ == '__main__':
    numbers = map_file_lines(lambda x: x)
    largest_magnitude = 0
    for number_1 in numbers:
        for number_2 in numbers:
            if number_2 == number_1:
                continue
            largest_magnitude = max(largest_magnitude, calculate_magnitude(add_numbers(number_1, number_2)))
    print(largest_magnitude)
