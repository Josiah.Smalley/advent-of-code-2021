import sys
from Common.FileHelper import map_file_lines
from itertools import product


def step_grid(grid, num_rows, num_cols):
    new_grid = []
    flashed = set()
    to_flash = set()

    for r in range(num_rows):
        new_row = []
        for c in range(num_cols):
            v = grid[r][c] + 1
            new_row.append(v)
            if v > 9:
                to_flash.add((r, c))
        new_grid.append(new_row)

    while len(to_flash) > 0:
        row, col = current = to_flash.pop()
        flashed.add(current)
        rows = [dr for dr in range(row - 1, row + 2) if 0 <= dr < num_rows]
        cols = [dc for dc in range(col - 1, col + 2) if 0 <= dc < num_cols]
        for r, c in product(rows, cols):
            visited = (r, c)
            new_grid[r][c] += 1
            if new_grid[r][c] > 9 and visited not in flashed and visited not in to_flash:
                to_flash.add(visited)

    for r, c in flashed:
        new_grid[r][c] = 0

    return new_grid, len(flashed)


def print_grid(grid):
    for row in grid:
        for col in row:
            print(col, end=' ')
        print('')
    print('')


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception("Second parameter must be the number of steps")

    steps = int(sys.argv[2])

    the_grid = map_file_lines(lambda line: [int(c) for c in line])
    row_count = len(the_grid)
    col_count = len(the_grid[0])

    total_flashed = 0

    for _ in range(steps):
        the_grid, num_flashed = step_grid(the_grid, row_count, col_count)
        total_flashed += num_flashed

    print_grid(the_grid)
    print("\n", total_flashed)
