from Common.FileHelper import map_file_lines
from functools import reduce


def string_to_digit_array(string):
    return list(map(int, string))


def is_smaller_than_surrounding(the_grid, row, col, max_row, max_col):
    value = the_grid[row][col]
    if row > 0 and value >= the_grid[row - 1][col]:
        return False
    if row + 1 < max_row and value >= the_grid[row + 1][col]:
        return False
    if col > 0 and value >= the_grid[row][col - 1]:
        return False
    if col + 1 < max_col and value >= the_grid[row][col + 1]:
        return False

    return True


def get_size_of_basin(the_grid, first_row, first_col, max_row, max_col):
    seen = set()
    to_visit = {(first_row, first_col)}
    while len(to_visit) > 0:
        current = to_visit.pop()
        seen.add(current)
        row, col = current

        for v in (-1, 1):
            if 0 <= row + v < max_row and the_grid[row + v][col] != 9:
                neighbor = (row + v, col)
                if neighbor not in seen:
                    to_visit.add(neighbor)
            if 0 <= col + v < max_col and the_grid[row][col + v] != 9:
                neighbor = (row, col + v)
                if neighbor not in seen:
                    to_visit.add(neighbor)
    return len(seen)


if __name__ == '__main__':
    grid = map_file_lines(string_to_digit_array)
    num_rows = len(grid)
    num_cols = len(grid[0])
    basin_sizes = list()

    for i in range(num_rows):
        for j in range(num_cols):
            if is_smaller_than_surrounding(grid, i, j, num_rows, num_cols):
                basin_sizes.append(get_size_of_basin(grid, i, j, num_rows, num_cols))

    print(reduce(lambda a, b: a * b, sorted(basin_sizes, reverse=True)[:3], 1))
