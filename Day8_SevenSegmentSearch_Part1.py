import sys
from collections import defaultdict

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    total = 0

    with open(file_path, 'r') as f:
        for line in f:
            _, display = line.strip().split(' | ')
            split_line = display.split(' ')
            total += len(list(filter(lambda x: len(x) in (2, 3, 4, 7), display.split(' '))))

    print(total)
