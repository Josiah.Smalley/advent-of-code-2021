import sys

from Common.FileHelper import map_file_lines
from Common.PriorityQueue import PriorityQueue
from itertools import product

if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception("Parameters must be: <input filename> <map size factor>")

    map_factor = int(sys.argv[2])
    original_risk = map_file_lines(lambda line: [int(c) for c in line])
    original_size = len(original_risk)
    size = map_factor * original_size

    source = (0, 0)
    target = (size - 1, size - 1)

    risk = [[0 for _ in range(size)] for __ in range(size)]
    pq = PriorityQueue()
    distances = {source: 0}
    previous = dict()

    for coord in product(range(size), range(size)):
        if coord != source:
            distances[coord] = float('inf')
            previous[coord] = None
        pq.add_element(coord, distances[coord])

        i, j = coord
        downward, original_i = divmod(i, original_size)
        rightward, original_j = divmod(j, original_size)
        shift = downward + rightward
        value = original_risk[original_i][original_j] + shift
        while value > 9:
            value -= 9
        risk[i][j] = value

    iterations = 0
    while not pq.is_empty():
        i, j = coord = pq.pop_element()
        # print(coord)
        for neighbor in [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]:
            ni, nj = neighbor
            if not (0 <= ni < size and 0 <= nj < size):
                continue
            neighbor_risk = risk[ni][nj]
            # print('\t', neighbor)
            distance = distances[coord] + neighbor_risk
            if distance < distances[neighbor]:
                distances[neighbor] = distance
                previous[neighbor] = coord
                pq.add_element(neighbor, distance)
        iterations += 1
        if coord == target:
            break

    print(f'Risk of {distances[target]} found in {iterations} iterations')
