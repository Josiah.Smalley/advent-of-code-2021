import sys
from functools import reduce


def count_bits(aggregate, bit_list):
    return list(map(
        lambda zipped: [zipped[0][0] + zipped[1][0], zipped[0][1] + zipped[1][1]],
        zip(aggregate, bit_list)
    ))


def counts_to_gamma_binary(counts):
    return ''.join(map(lambda c: str(0 if c[0] > c[1] else 1), counts))


def counts_to_epsilon_binary(counts):
    return ''.join(map(lambda c: str(0 if c[0] < c[1] else 1), counts))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    data = []
    with open(file_path, 'r') as f:
        is_first = True
        for line in f:
            data.append(list(map(lambda bit: [1 - bit, bit], map(int, line.strip()))))

    counts = reduce(count_bits, data, [[0, 0] for _ in data[0]])
    gamma_bin = counts_to_gamma_binary(counts)
    epsilon_bin = counts_to_epsilon_binary(counts)

    print(int(gamma_bin, 2) * int(epsilon_bin, 2))
