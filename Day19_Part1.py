from __future__ import annotations

import re
from itertools import combinations, product
from typing import Union, Set, Tuple, Dict, List
from random import shuffle

import numpy as np

from Common.DisjointSet import DisjointSet
from Common.FileHelper import iterate_over_file

rotation_matrices = []

for axis_1, axis_2 in combinations(((1, 0, 0), (0, 1, 0), (0, 0, 1)), r=2):
    for f1, f2 in product((1, -1), (1, -1)):
        forward = np.multiply(axis_1, f1)
        left = np.multiply(axis_2, f2)
        up = np.cross(forward, left)
        rotation_matrices.append(np.mat((forward, left, up)))
        up = np.cross(left, forward)
        rotation_matrices.append(np.mat((left, forward, up)))


class Scanner(object):
    rotations: Union[None, Tuple[int, int, int]]
    data: Dict[Tuple[int, int, int], Set[Tuple[int, int, int]]]
    relative_probes: Set[Tuple[int, int, int]]
    probe_mat: np.matrix
    rotated_probes: List[np.matrix]
    sorted_rotated_probes: Tuple[Tuple[Tuple[int, int, int]]]

    def __init__(self, points: Set[Tuple[int, int, int]]):
        self.relative_probes = points
        self.probe_mat = np.mat(list(points))
        self.rotated_probes = list(
            map(lambda rotation: self.probe_mat.dot(rotation), rotation_matrices)
        )
        self.sorted_rotated_probes = tuple(
            map(lambda rotated_probes: tuple(
                sorted(map(Scanner.row_to_tuple, rotated_probes))),
                self.rotated_probes)
        )

    @staticmethod
    def row_to_tuple(row: np.mat) -> Tuple[int, int, int]:
        return row[0, 0], row[0, 1], row[0, 2]

    def overlaps(self, other: Scanner) -> Tuple[bool, Union[None, int], Union[None, np.matrix]]:
        # todo extend
        for i in range(24):
            # print('Rotation', i)
            does_overlap, translation = self.overlaps_with_rotation(other, i)
            # print('\n')
            if does_overlap:
                return True, i, translation
        return False, None, None

    def overlaps_with_rotation(
            self,
            other: Scanner,
            rotation_index: int,
            needed_shared: int = 12) -> Tuple[bool, Union[None, Tuple[int, int, int]]]:
        my_probes = self.sorted_rotated_probes[0]
        other_probes = other.sorted_rotated_probes[rotation_index]

        can_map, the_offset = Scanner.can_translate_onto(my_probes, other_probes, needed_shared)
        if can_map:
            return True, the_offset
        #
        # rotated_other: np.matrix = other.rotated_probes[rotation_index]
        # my_length = len(self.sorted_rotated_probes[0])
        # other_length = len(other.sorted_rotated_probes[rotation_index])
        #
        # my_rows = self.sorted_rotated_probes[0] if my_length > other_length else self.sorted_rotated_probes[0][:-11]
        # other_rows = other.sorted_rotated_probes[rotation_index] if other_length >= my_length else \
        #     other.sorted_rotated_probes[rotation_index][:-11]
        #
        # for my_row in my_rows:
        #     for row in other_rows:
        #         diff: Tuple[int, int, int] = tuple((int(my_row[k] - row[k]) for k in range(3)))
        #         if any(map(lambda x: abs(x) > 2000, diff)):
        #             continue
        #
        #         if self.has_twelve_in_common(other.sorted_rotated_probes[rotation_index], diff):
        #             return True, diff
        #         # num_common = self.get_num_common_probes_after_translation(rotated_other, diff)
        #         # if num_common >= 12:
        #         #     return True, diff
        return False, None

    @staticmethod
    def can_translate_onto(first: Tuple[Tuple[int, int, int]], second: Tuple[Tuple[int, int, int]],
                           needed_shared: int, depth: int = 0, have_checked: set = None) -> Tuple[bool, Union[None, Tuple[int, int, int]]]:
        if len(first) < needed_shared or len(second) < needed_shared:
            return False, None

        have_checked = have_checked if have_checked is not None else set()

        key = (len(first), len(second))
        if key in have_checked:
            return False, None

        have_checked.add(key)

        # print('\t'*depth,depth, key, len(have_checked))

        # option 1: the smallest tuples are mapped to each other
        can_translate, trans = Scanner.can_translate_onto_using_first_elements(first, second, needed_shared)
        if can_translate:
            return True, trans

        # option 2: the smallest tuple from the first is not a common probe
        can_translate, trans = Scanner.can_translate_onto(first[1:], second, needed_shared, depth+1, have_checked)
        if can_translate:
            return True, trans

        # option 3: the smallest tuple from the second is not a common probe
        can_translate, trans = Scanner.can_translate_onto(first, second[1:], needed_shared, depth+1, have_checked)
        if can_translate:
            return True, trans

        # no other options
        return False, None

    @staticmethod
    def can_translate_onto_using_first_elements(first: Tuple[Tuple[int, int, int]], second: Tuple[Tuple[int, int, int]],
                                                needed_shared: int) -> Tuple[bool, Union[None, Tuple[int, int, int]]]:
        # print(first, second, needed_shared)
        num_shared = 1
        first_index = 1
        first_size = len(first)
        second_index = 1
        second_size = len(second)

        offset: Tuple[int, int, int] = tuple(second[0][i] - first[0][i] for i in range(3))

        while first_index < first_size and second_index < second_size:
            # print(first_index, first_size, second_index, second_size)
            translated = tuple(first[first_index][i] + offset[i] for i in range(3))

            if second[second_index] == translated:
                num_shared += 1
                if num_shared >= needed_shared:
                    return True, offset
                first_index += 1
                second_index += 1
            elif second[second_index] < translated:
                second_index += 1
            else:
                first_index += 1

        return False, None

    def has_twelve_in_common(self,
                             other: Tuple[Tuple[Tuple[int, int, int]]],
                             offset: Tuple[int, int, int]) -> bool:
        mine = self.sorted_rotated_probes[0]
        my_index = 0
        my_max = len(mine)
        other_index = 0
        other_max = len(other)

        intersected = 0

        while my_index < my_max and other_index < other_max:
            translated = tuple(other[other_index][k] + offset[k] for k in range(3))
            # print('Translated:', translated)
            if mine[my_index] == translated:
                # print("Translated:", mine[my_index], ",", other[other_index], "with", offset)
                intersected += 1
                if intersected >= 12:
                    return True
                my_index += 1
                other_index += 1
            elif mine[my_index] < translated:
                my_index += 1
            else:
                other_index += 1
        # print(intersected)
        return False

    @staticmethod
    def are_offset(first: Tuple[int, int, int], second: Tuple[int, int, int], offset: Tuple[int, int, int]) -> bool:
        return all(map(lambda p: p[0] + p[2] == p[1], zip(first, second, offset)))

    def get_num_common_probes_after_translation(self, other_matrix: np.matrix, offset: np.matrix):
        translated_matrix = Scanner.translate_matrix(other_matrix, offset)
        # return len(tuple(filter(lambda x: x in translated_matrix, self.probe_mat)))
        return len([row for row in self.probe_mat if row in translated_matrix])
        # intersection_size = 0
        # for row in self.probe_mat:
        #     if row in translated_matrix:
        #         intersection_size += 1
        # return intersection_size
        # return len(Scanner.get_translated_set(other_matrix, offset).intersection(self.relative_probes))

    @staticmethod
    def get_translated_set(matrix: np.matrix, offset: np.matrix):
        points = set()
        for r in Scanner.translate_matrix(matrix, offset):
            points.add(set(map(tuple, np.array(r))).pop())
        return points

    @staticmethod
    def translate_matrix(matrix: np.matrix, offset: np.matrix):
        return matrix + offset


class ScannerData(object):
    scanners: List[Scanner]
    data: dict
    current_set: Union[set, None]
    current_scanner: Union[int, None]

    def __init__(self):
        self.scanners = []
        self.data = dict()
        self.current_set = None
        self.current_scanner = None

    def process_line(self, line: str) -> None:
        if line == '':
            if self.current_scanner is None:
                return
            self.data[self.current_scanner] = self.current_set
            self.scanners.append(Scanner(self.current_set))
            self.current_scanner = None
            self.current_set = None
            return

        match: re.Match = re.match(r"^--- scanner (\d+) ---$", line)
        if match:
            self.current_set = set()
            self.current_scanner = int(match.group(1))
            return

        match = re.match(r"^(-?\d+),(-?\d+),(-?\d+)$", line)
        if not match:
            raise Exception("Unable to parse line: " + line)

        self.current_set.add(tuple(map(int, match.groups())))


def check_scanners(scanners: List[Scanner], indices: Tuple[int, int]):
    return scanners[indices[0]].overlaps(scanners[indices[1]])


if __name__ == '__main__':
    scanner_data = ScannerData()
    iterate_over_file(scanner_data.process_line)
    scanner_data.process_line('')  # append a blank line to the file

    # print('Done initializing')
    # print(scanner_data.scanners[0].rotated_probes)

    # print(0, 1, scanner_data.scanners[0].overlaps(scanner_data.scanners[1]))

    num_scanners = len(scanner_data.scanners)
    pairs = list((i, j) for i in range(num_scanners) for j in range(i + 1, num_scanners))
    shuffle(pairs)

    ds = DisjointSet()
    for n in range(num_scanners):
        ds.make_set(n)

    num_edges = 0
    edges = {n: dict() for n in range(num_scanners)}
    for i, j in pairs:
        if ds.are_connected(i, j):
            print(f'{i} and {j} are already connected. Skipping...')
            continue
        do_overlap, rot_mat_index, translation = scanner_data.scanners[i].overlaps(scanner_data.scanners[j])
        if do_overlap:
            ds.merge(i, j)
            num_edges += 1
            edges[i][j] = (rot_mat_index, translation, True)
            edges[j][i] = (rot_mat_index, translation, False)
            print(f'{i} and {j} are now connected. {num_edges} have been found of the needed {num_scanners - 1}')
            if num_edges >= num_scanners - 1:
                print('Necessary edges have been found')
                break

    probes = set()
    seen = set()
    queue = [(0, np.identity(3), (0, 0, 0))]
    while queue:
        scanner_index, current_rotation, current_translation = queue.pop()
        if scanner_index in seen:
            continue
        print('\n\n',scanner_index, current_translation)
        print('current rotation:\n', current_rotation)
        print('current rotation inverse:\n', np.linalg.inv(current_rotation))
        seen.add(scanner_index)

        for neighbor_scanner_index in edges[scanner_index].keys():
            if neighbor_scanner_index in seen:
                continue

            rotation_index_to_add, translation_to_add, is_forward = edges[scanner_index][neighbor_scanner_index]
            print('Neighbor #' + str(neighbor_scanner_index))
            print('Is forward?', 'yes' if is_forward else 'no')
            print('Translation to add:\n', translation_to_add)
            relative_rotation = rotation_matrices[rotation_index_to_add]
            print('Relative rotation:\n', relative_rotation)
            print('Relative rotation inverse:\n', np.linalg.inv(relative_rotation))
            print('Multiplied:\n', np.dot(relative_rotation, np.linalg.inv(relative_rotation)))
            rotation_to_add = rotation_matrices[rotation_index_to_add]
            rotation_to_add = rotation_to_add if is_forward else np.linalg.inv(rotation_to_add)
            print('Rotation to add:\n', rotation_to_add)
            new_rotation = current_rotation.dot(rotation_to_add)
            print('New rotation:\n',new_rotation)
            new_translation = list(current_translation)
            if is_forward:
                translation_to_add = np.mat(translation_to_add).dot(np.linalg.inv(current_rotation))
            else:
                translation_to_add = np.linalg.inv(current_rotation).dot(np.mat(translation_to_add).T).T
            # translation_to_add = np.mat(translation_to_add).dot(np.linalg.inv(current_rotation))
            print('Translation to add:\n', translation_to_add)
            print('New translation before:\n', new_translation)
            for i in range(3):
                new_translation[i] += -translation_to_add[0, i] if is_forward else translation_to_add[0, i]
            print('New translation after:\n', new_translation)

            queue.append((neighbor_scanner_index, new_rotation, tuple(new_translation)))
