import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    aim = 0
    horizontal = 0
    depth = 0
    with open(file_path, 'r') as f:
        for line in f:
            [direction, value] = line.split(' ')
            value = int(value)
            if direction == 'forward':
                horizontal += value
                depth += value * aim
            else:
                aim += value * (1 if direction == 'down' else -1)

    print(horizontal * depth)
