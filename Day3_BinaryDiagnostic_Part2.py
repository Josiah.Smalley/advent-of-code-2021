import sys
from functools import reduce


def find_oxygen_rating(values):
    return find_rating(values, True)


def find_co2_rating(values):
    return find_rating(values, False)


def find_rating(values, favor_greater, index=0):
    if len(values) == 1:
        return int(values[0], 2)

    counts = reduce(
        lambda aggregate, current: [aggregate[0] + 1 - current, aggregate[1] + current],
        map(lambda v: int(v[index]), values),
        [0, 0]
    )

    keep_if = choose_value_to_keep(counts, favor_greater)
    return find_rating(
        list(filter(lambda v: v[index] == keep_if, values)),
        favor_greater,
        index + 1
    )


def choose_value_to_keep(counts, favor_greater):
    if favor_greater:
        return '1' if counts[1] >= counts[0] else '0'

    return '0' if counts[0] <= counts[1] else '1'


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    data = []
    with open(file_path, 'r') as f:
        is_first = True
        for line in f:
            data.append(line.strip())

    print(find_oxygen_rating(data) * find_co2_rating(data))
