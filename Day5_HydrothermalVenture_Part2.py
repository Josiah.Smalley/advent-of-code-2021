import sys
import re
from functools import reduce

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File path must be passed in")
    file_path = sys.argv[1]

    if not file_path:
        raise Exception("File path must be defined")

    max_x = 0
    max_y = 0
    lines = []
    with open(file_path, 'r') as f:
        for line in f:
            [x1, y1, x2, y2] = map(int, re.findall(r'\d+', line))
            if x2 < x1 or (x2 == x1 and y2 < y1):
                x1, y1, x2, y2 = x2, y2, x1, y1
            max_x = max(max_x, x1, x2)
            max_y = max(max_y, y1, y2)

            lines.append((x1, y1, x2, y2))

    grid = [[0 for _ in range(max_y + 1)] for __ in range(max_x + 1)]

    for x1, y1, x2, y2 in lines:
        if x1 == x2:
            for y in range(y1, y2 + 1):
                grid[x1][y] += 1
        elif y1 == y2:
            for x in range(x1, x2 + 1):
                grid[x][y1] += 1
        else:
            x = x1
            dx = 1 if x1 < x2 else -1
            y = y1
            dy = 1 if y1 < y2 else -1
            while x != x2:
                grid[x][y] += 1
                x += dx
                y += dy
            grid[x2][y2] += 1

    row_sums = map(lambda r: reduce(lambda acc, curr: acc + (1 if curr > 1 else 0), r, 0), grid)
    print(sum(row_sums))
