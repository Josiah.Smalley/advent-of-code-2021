from __future__ import annotations

import re
from itertools import combinations, product
from typing import Union, Set, Tuple, Dict, List
from random import shuffle

import numpy as np

from Common.DisjointSet import DisjointSet
from Common.FileHelper import iterate_over_file

rotation_matrices = []

for axis_1, axis_2 in combinations(((1, 0, 0), (0, 1, 0), (0, 0, 1)), r=2):
    for f1, f2 in product((1, -1), (1, -1)):
        forward = np.multiply(axis_1, f1)
        left = np.multiply(axis_2, f2)
        up = np.cross(forward, left)
        rotation_matrices.append(np.mat((forward, left, up)))
        up = np.cross(left, forward)
        rotation_matrices.append(np.mat((left, forward, up)))


class Scanner(object):
    rotations: Union[None, Tuple[int, int, int]]
    data: Dict[Tuple[int, int, int], Set[Tuple[int, int, int]]]
    relative_probes: Set[Tuple[int, int, int]]
    probe_mat: np.matrix
    rotated_probes: List[np.matrix]
    sorted_rotated_probes: Tuple[Tuple[Tuple[int, int, int]]]

    relative_subsets: List[Dict[Tuple[int, int, int], Set[Tuple[int, int, int]]]]

    def __init__(self, points: Set[Tuple[int, int, int]]):
        self.relative_probes = points
        self.probe_mat = np.mat(list(points))
        self.rotated_probes = list(
            map(lambda rotation: self.probe_mat.dot(rotation), rotation_matrices)
        )
        self.sorted_rotated_probes = tuple(
            map(lambda rotated_probes: tuple(
                sorted(map(Scanner.row_to_tuple, rotated_probes))),
                self.rotated_probes)
        )
        self.relative_subsets = list(
            map(
                lambda sorted_rotated_probes: Scanner.generate_valid_relative_map(set(sorted_rotated_probes)),
                self.sorted_rotated_probes
            )
        )

    @staticmethod
    def generate_valid_relative_map(
            points: Set[Tuple[int, int, int]]
    ) -> Dict[Tuple[int, int, int], Set[Tuple[int, int, int]]]:
        return Scanner.generate_relative_map(points)
        # result = Scanner.generate_relative_map(points)
        # keys = list(result.keys())
        # for key in keys:
        #     if len(result[key]) < 12:
        #         del (result[key])
        #
        # return result

    @staticmethod
    def generate_relative_map(
            points: Set[Tuple[int, int, int]]
    ) -> Dict[Tuple[int, int, int], Set[Tuple[int, int, int]]]:
        result = dict()
        ordered_tuples = list(sorted(points))

        length = len(ordered_tuples)
        for i in range(length):
            source: Tuple[int, int, int] = ordered_tuples[i]
            a, b, c = source
            relative_to_source = set()

            for j in range(i, length):
                target: Tuple[int, int, int] = ordered_tuples[j]
                r, s, t = target
                relative_position: Tuple[int, int, int] = (r - a, s - b, t - c)
                # if all(map(lambda x: x >= 0, relative_position)):
                relative_to_source.add(relative_position)

            result[source] = relative_to_source

        return result

    @staticmethod
    def row_to_tuple(row: np.mat) -> Tuple[int, int, int]:
        return row[0, 0], row[0, 1], row[0, 2]

    def overlaps(self, other: Scanner) -> Tuple[bool, Union[None, int], Union[None, np.matrix]]:
        # todo extend
        for i in range(24):
            # print('Rotation', i)
            does_overlap, translation = self.overlaps_with_rotation(other, i)
            # print('\n')
            if does_overlap:
                return True, i, translation
        return False, None, None

    def overlaps_with_rotation(
            self,
            other: Scanner,
            rotation_index: int) -> Tuple[bool, Union[None, Tuple[int, int, int]]]:

        my_valid_options = self.relative_subsets[0]
        other_valid_options = other.relative_subsets[rotation_index]

        for my_source, my_relatives in my_valid_options.items():
            # print(my_source)
            # print('\t', my_relatives)
            for other_source, other_relatives in other_valid_options.items():
                # print('\t\t', other_source)
                # print('\t\t\t', other_relatives)
                if len(my_relatives.intersection(other_relatives)) >= 12:
                    mx, my, mz = my_source
                    ox, oy, oz = other_source
                    offset = (-ox + mx, -oy + my, -oz + mz)
                    return True, offset

        return False, None

    @staticmethod
    def can_translate_onto(first: Tuple[Tuple[int, int, int]], second: Tuple[Tuple[int, int, int]],
                           needed_shared: int, depth: int = 0, have_checked: set = None) -> Tuple[
        bool, Union[None, Tuple[int, int, int]]]:
        if len(first) < needed_shared or len(second) < needed_shared:
            return False, None

        have_checked = have_checked if have_checked is not None else set()

        key = (len(first), len(second))
        if key in have_checked:
            return False, None

        have_checked.add(key)

        # print('\t'*depth,depth, key, len(have_checked))

        # option 1: the smallest tuples are mapped to each other
        can_translate, trans = Scanner.can_translate_onto_using_first_elements(first, second, needed_shared)
        if can_translate:
            return True, trans

        # option 2: the smallest tuple from the first is not a common probe
        can_translate, trans = Scanner.can_translate_onto(first[1:], second, needed_shared, depth + 1, have_checked)
        if can_translate:
            return True, trans

        # option 3: the smallest tuple from the second is not a common probe
        can_translate, trans = Scanner.can_translate_onto(first, second[1:], needed_shared, depth + 1, have_checked)
        if can_translate:
            return True, trans

        # no other options
        return False, None

    @staticmethod
    def can_translate_onto_using_first_elements(first: Tuple[Tuple[int, int, int]], second: Tuple[Tuple[int, int, int]],
                                                needed_shared: int) -> Tuple[bool, Union[None, Tuple[int, int, int]]]:
        # print(first, second, needed_shared)
        num_shared = 1
        first_index = 1
        first_size = len(first)
        second_index = 1
        second_size = len(second)

        offset: Tuple[int, int, int] = tuple(second[0][i] - first[0][i] for i in range(3))

        while first_index < first_size and second_index < second_size:
            # print(first_index, first_size, second_index, second_size)
            translated = tuple(first[first_index][i] + offset[i] for i in range(3))

            if second[second_index] == translated:
                num_shared += 1
                if num_shared >= needed_shared:
                    return True, offset
                first_index += 1
                second_index += 1
            elif second[second_index] < translated:
                second_index += 1
            else:
                first_index += 1

        return False, None

    def has_twelve_in_common(self,
                             other: Tuple[Tuple[Tuple[int, int, int]]],
                             offset: Tuple[int, int, int]) -> bool:
        mine = self.sorted_rotated_probes[0]
        my_index = 0
        my_max = len(mine)
        other_index = 0
        other_max = len(other)

        intersected = 0

        while my_index < my_max and other_index < other_max:
            translated = tuple(other[other_index][k] + offset[k] for k in range(3))
            # print('Translated:', translated)
            if mine[my_index] == translated:
                # print("Translated:", mine[my_index], ",", other[other_index], "with", offset)
                intersected += 1
                if intersected >= 12:
                    return True
                my_index += 1
                other_index += 1
            elif mine[my_index] < translated:
                my_index += 1
            else:
                other_index += 1
        # print(intersected)
        return False

    @staticmethod
    def are_offset(first: Tuple[int, int, int], second: Tuple[int, int, int], offset: Tuple[int, int, int]) -> bool:
        return all(map(lambda p: p[0] + p[2] == p[1], zip(first, second, offset)))

    def get_num_common_probes_after_translation(self, other_matrix: np.matrix, offset: np.matrix):
        translated_matrix = Scanner.translate_matrix(other_matrix, offset)
        # return len(tuple(filter(lambda x: x in translated_matrix, self.probe_mat)))
        return len([row for row in self.probe_mat if row in translated_matrix])
        # intersection_size = 0
        # for row in self.probe_mat:
        #     if row in translated_matrix:
        #         intersection_size += 1
        # return intersection_size
        # return len(Scanner.get_translated_set(other_matrix, offset).intersection(self.relative_probes))

    @staticmethod
    def get_translated_set(matrix: np.matrix, offset: np.matrix):
        points = set()
        for r in Scanner.translate_matrix(matrix, offset):
            points.add(set(map(tuple, np.array(r))).pop())
        return points

    @staticmethod
    def translate_matrix(matrix: np.matrix, offset: np.matrix):
        return matrix + offset


class ScannerData(object):
    scanners: List[Scanner]
    data: dict
    current_set: Union[set, None]
    current_scanner: Union[int, None]

    def __init__(self):
        self.scanners = []
        self.data = dict()
        self.current_set = None
        self.current_scanner = None

    def process_line(self, line: str) -> None:
        if line == '':
            if self.current_scanner is None:
                return
            self.data[self.current_scanner] = self.current_set
            self.scanners.append(Scanner(self.current_set))
            self.current_scanner = None
            self.current_set = None
            return

        match: re.Match = re.match(r"^--- scanner (\d+) ---$", line)
        if match:
            self.current_set = set()
            self.current_scanner = int(match.group(1))
            return

        match = re.match(r"^(-?\d+),(-?\d+),(-?\d+)$", line)
        if not match:
            raise Exception("Unable to parse line: " + line)

        self.current_set.add(tuple(map(int, match.groups())))


def check_scanners(scanners: List[Scanner], indices: Tuple[int, int]):
    return scanners[indices[0]].overlaps(scanners[indices[1]])


if __name__ == '__main__':
    scanner_data = ScannerData()
    iterate_over_file(scanner_data.process_line)
    scanner_data.process_line('')  # append a blank line to the file

    # print('done')

    # print(scanner_data.scanners[0].overlaps(scanner_data.scanners[1]))
    # num_scanners = len(scanner_data.scanners)
    # for i in range(num_scanners):
    #     for j in range(i+1, num_scanners):
    #         print(i, j, scanner_data.scanners[i].overlaps(scanner_data.scanners[j]))

    # print('Done initializing')
    # print(scanner_data.scanners[0].rotated_probes)

    # print(0, 1, scanner_data.scanners[0].overlaps(scanner_data.scanners[1]))

    num_scanners = len(scanner_data.scanners)
    pairs = list((i, j) for i in range(num_scanners) for j in range(i + 1, num_scanners))
    shuffle(pairs)

    ds = DisjointSet()
    for n in range(num_scanners):
        ds.make_set(n)

    num_edges = 0
    edges = {n: dict() for n in range(num_scanners)}
    for i, j in pairs:
        if ds.are_connected(i, j):
            print(f'{i} and {j} are already connected. Skipping...')
            continue
        do_overlap, rot_mat_index, translation = scanner_data.scanners[i].overlaps(scanner_data.scanners[j])
        if do_overlap:
            ds.merge(i, j)
            num_edges += 1
            edges[i][j] = (rot_mat_index, translation, True)
            edges[j][i] = (rot_mat_index, translation, False)
            print(f'{i} and {j} are now connected. {num_edges} have been found of the needed {num_scanners - 1}')
            if num_edges >= num_scanners - 1:
                print('Necessary edges have been found')
                break

    probes = set()
    seen = set()
    queue = [(0, np.identity(3), (0, 0, 0))]
    while queue:
        scanner_index, current_rotation, current_translation = queue.pop()
        if scanner_index in seen:
            continue
        # print('\n\n', scanner_index, current_translation)
        # print(current_rotation)
        seen.add(scanner_index)

        # add the probes for this scanner
        translation_matrix = np.asmatrix(current_translation)
        scanner = scanner_data.scanners[scanner_index]
        probe_matrix = scanner.probe_mat.dot(current_rotation) + translation_matrix
        for row in probe_matrix:
            probes.add(Scanner.row_to_tuple(row))

        for neighbor_scanner_index in edges[scanner_index].keys():
            if neighbor_scanner_index in seen:
                continue

            edge_rotation_index, edge_translation, is_edge_forward = edges[scanner_index][neighbor_scanner_index]
            edge_rotation = rotation_matrices[edge_rotation_index]
            # print('Neighbor #' + str(neighbor_scanner_index))
            # print('Is forward?', 'yes' if is_edge_forward else 'no')

            # print('Current translation\n', current_translation)
            if is_edge_forward:
                translation_offset = np.dot(edge_translation, current_rotation)
                new_rotation = edge_rotation.dot(current_rotation)
            else:
                translation_offset = np.dot(-np.array(edge_translation),
                                            np.linalg.inv(edge_rotation).dot(current_rotation))
                new_rotation = np.linalg.inv(edge_rotation).dot(current_rotation)

            # print("Calculated offset\n", translation_offset)
            new_translation = translation_matrix + translation_offset
            new_translation = Scanner.row_to_tuple(new_translation[0])
            # print('New translation', new_translation)
            # print('New rotation', new_rotation)

            queue.append((neighbor_scanner_index, new_rotation, tuple(new_translation)))

    print('Found a total of', len(probes), 'beacons')
