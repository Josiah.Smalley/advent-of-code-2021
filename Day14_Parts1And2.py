import re
import sys

from Common.FileHelper import iterate_over_file
from collections import defaultdict
from functools import reduce

pair_counts = defaultdict(int)
mapping = dict()
last_char = []


def process_line(line):
    if line == '':
        return

    pair_match = re.match(r"^(.+) -> (.+)$", line)
    if pair_match is not None:
        key, value = pair_match.groups()
        mapping[key] = value
        return

    for i in range(len(line) - 1):
        pair_counts[line[i:i + 2]] += 1

    last_char.append(line[-1])


def perform_step(counts):
    new_pairs = defaultdict(int)
    for key, value in counts.items():
        if key not in mapping:
            new_pairs[key] += value
            continue

        new_pairs[key[0] + mapping[key]] += value
        new_pairs[mapping[key] + key[1]] += value

    return new_pairs


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception("The second parameter must be the number of steps to perform")

    iterate_over_file(process_line)
    for _ in range(int(sys.argv[2])):
        pair_counts = perform_step(pair_counts)

    char_counts = defaultdict(int)
    char_counts[last_char[0]] = 1
    for k, v in pair_counts.items():
        char_counts[k[0]] += v

    min_count, max_count = reduce(
        lambda extrema, current: (min(extrema[0], current), max(extrema[1], current)),
        char_counts.values(),
        (float('inf'), 0)
    )

    print(max_count - min_count)
