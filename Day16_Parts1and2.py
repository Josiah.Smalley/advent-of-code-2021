from __future__ import annotations
import sys
from functools import reduce


class TransmissionReader(object):
    CHAR_MAP = {hex(v)[-1].upper(): bin(v)[2:].rjust(4, '0') for v in range(16)}

    def __init__(self, transmission: str):
        self.transmission = transmission
        self.length = len(transmission)
        self.index = 0
        self.nibble = None
        self.nibble_index = 4
        self.num_read = 0

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        if self.nibble_index > 3:
            if self.index >= self.length:
                raise StopIteration
            self.nibble_index = 0
            self.nibble = self.CHAR_MAP[self.transmission[self.index]]
            self.index += 1
        result = self.nibble[self.nibble_index]
        self.nibble_index += 1
        self.num_read += 1
        return result

    def read_literal(self):
        result = ''
        while True:
            group = self.read_bits(5)
            result += group[1:]
            if group[0] == '0':
                break
        return int(result, 2)

    def read_version(self):
        return self.read_bits(3, True)

    def read_type(self):
        return self.read_bits(3, True)

    def read_bits(self, num_bits: int, to_int=False):
        raw = ''.join(self.next() for _ in range(num_bits))
        return raw if not to_int else int(raw, 2)


class Packet(object):
    def __init__(self, version: int, packet_type: int):
        self.version = version
        self.packet_type = packet_type
        self.value = None
        self.sub_packets = []

    def is_literal(self) -> bool:
        return self.packet_type == 4

    @staticmethod
    def read_packet(transmission: TransmissionReader) -> Packet:
        packet = Packet(transmission.read_version(), transmission.read_type())
        if packet.is_literal():
            packet.value = transmission.read_literal()
        else:
            length_type = transmission.read_bits(1)
            if length_type == '0':
                num_sub_bits = transmission.read_bits(15, True)
                # print('Operator with subpacket length of:', num_sub_bits)
                target_bits = transmission.num_read + num_sub_bits
                while transmission.num_read < target_bits:
                    packet.sub_packets.append(Packet.read_packet(transmission))
            else:
                num_packets = transmission.read_bits(11, True)
                # print('Operator with number of subpackets:', num_packets)
                for _ in range(num_packets):
                    packet.sub_packets.append(Packet.read_packet(transmission))
            packet.get_value()
        return packet

    def get_value(self):
        if self.value is not None:
            return self.value

        sub_values = [x.get_value() for x in self.sub_packets]

        if self.packet_type == 0:
            self.value = sum(sub_values)
        elif self.packet_type == 1:
            self.value = reduce(lambda prod, x: prod * x, sub_values, 1)
        elif self.packet_type == 2:
            self.value = min(sub_values)
        elif self.packet_type == 3:
            self.value = max(sub_values)
        elif self.packet_type == 5:
            self.value = 1 if sub_values[0] > sub_values[1] else 0
        elif self.packet_type == 6:
            self.value = 1 if sub_values[0] < sub_values[1] else 0
        elif self.packet_type == 7:
            self.value = 1 if sub_values[0] == sub_values[1] else 0

        if self.value is None:
            raise Exception("Packet value could not be determined")

        return self.value

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self.to_string()

    def to_string(self, depth: int = 0):
        prefix = "  " * depth
        result = prefix + f'[[Version: {self.version}; Type: {self.packet_type}]]\n'
        if self.value is not None:
            result += prefix + f'  VALUE: {self.value}\n'
        result += '\n'.join([x.to_string(depth + 1) for x in self.sub_packets])
        return result

    def calculate_version_sum(self):
        return self.version + sum(x.calculate_version_sum() for x in self.sub_packets)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise Exception("You must pass the encoded transmission as the script parameter")
    incoming_transmission = sys.argv[1]
    reader = TransmissionReader(incoming_transmission)

    root = Packet.read_packet(reader)
    print(root)

    version_sum = root.calculate_version_sum()
    operation_result = root.get_value()

    print(f'The total version sum is: {version_sum}')
    print(f'The result of all applied operations is: {operation_result}')

    # print(''.join(TransmissionReader(incoming_transmission)))
